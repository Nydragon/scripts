argparse 'f/file=!test -e "$_flag_value"' -- $argv; or return

set pids $(pidof swaybg)

if set -q _flag_file
  swaybg -i "$(find $_flag_file | shuf -n 1)" > /dev/null 2>&1 &
else
  swaybg -i "$(find ~/Pictures/backgrounds | shuf -n 1)" > /dev/null 2>&1 &
end

sleep 0.5;

for i in $(string split " " $pids)
  echo "killing process $i";
  kill -9 "$i";
end
